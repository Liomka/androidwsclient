package io.liomka.androidwsclient.app.worker;

import org.glassfish.tyrus.client.ClientManager;
import org.json.JSONException;
import org.json.JSONObject;

import javax.websocket.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Observable;

/**
 * Created by Liomka on 01/07/2014.
 */
public class WebSocketsClient extends Observable {

    private IWebSocketsClient _listener;
    private Session _currentSession;
    private final WebSocketsClientSender _sender;

    // CSTR
    public WebSocketsClient(IWebSocketsClient listener, String serverURI) {
        _listener = listener;
        new WebSocketsClientReceiver(serverURI).start();
        _sender = new WebSocketsClientSender();
        _sender.start();
    }

    public void kill() {
        new Thread() {
            public void run() {
                try {
                    _currentSession.close(new CloseReason(CloseReason.CloseCodes.NORMAL_CLOSURE, ""));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    // Send methods
    public void sendMessage(String message) {
        try {
            send("message", new JSONObject().put(
                    "content", message));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void sendHello(String name) {
        try {
            send("hello", new JSONObject().put(
                    "name", name));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // Privates methods
    private void send(String method, JSONObject content) {
        try {
            _sender.send(new JSONObject().put(
                    method, content));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* --------------------------------------------------------------------- */
    /* ------------------------ IWebSocketsClient -------------------------- */
    /* --------------------------------------------------------------------- */
    public interface IWebSocketsClient {
        public void onWebSocketsConnected(JSONObject time);
        public void onWebSocketsDisconnect(JSONObject time);

        public void onMessageReceived(JSONObject message);
    }

    /* --------------------------------------------------------------------- */
    /* --------------------- WebSocketsClientSender ------------------------ */
    /* --------------------------------------------------------------------- */
    private class WebSocketsClientSender extends Thread {

        private ArrayList<JSONObject> _queuedList;

        public WebSocketsClientSender() {
            _queuedList = new ArrayList<JSONObject>();
        }

        public void send(JSONObject object) {
            _queuedList.add(object);
            synchronized(_sender) {
                _sender.notify();
            }
        }

        @Override
        public void run() {
            super.run();
            do {
                if (_currentSession != null) {
                    try {
                        _currentSession.getBasicRemote().sendObject(_queuedList.remove(0));
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (EncodeException e) {
                        e.printStackTrace();
                    }
                }

                if (_queuedList.isEmpty()) {
                    synchronized (this) {
                        try {
                            _sender.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } while (_currentSession != null);

            System.out.println("I'm not listening you");
        }
    }

    /* --------------------------------------------------------------------- */
    /* --------------------- WebSocketsClientReceiver ---------------------- */
    /* --------------------------------------------------------------------- */
    private class WebSocketsClientReceiver extends Thread {

        private String _serverURI;

        public WebSocketsClientReceiver(String serverURI) {
            _serverURI = serverURI;
        }

        @Override
        public void run() {
            super.run();
            final ClientEndpointConfig cec = ClientEndpointConfig.Builder.create().build();

            ClientManager client = ClientManager.createClient();
            try {
                client.connectToServer(new Endpoint() {

                    @Override
                    public void onOpen(Session session, EndpointConfig config) {
                        session.addMessageHandler(new MessageHandler.Whole<String>() {
                            @Override
                            public void onMessage(String message) {
                                try {
                                    JSONObject jmessage = new JSONObject(message);
                                    if (jmessage.has("accepted"))
                                        _listener.onWebSocketsConnected(jmessage.getJSONObject("accepted"));
                                    else if (jmessage.has("rejected"))
                                        _listener.onWebSocketsDisconnect(jmessage.getJSONObject("rejected"));
                                    else if (jmessage.has("message"))
                                        _listener.onMessageReceived(jmessage.getJSONObject("message"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        _currentSession = session;
                        sendHello("Android device");
                    }

                    @Override
                    public void onClose(Session session, CloseReason closeReason) {
                        try {
                            System.out.println("onClose");
                            _currentSession = null;
                            _listener.onWebSocketsDisconnect(new JSONObject().put("time", System.currentTimeMillis()/1000));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, cec, new URI(_serverURI));
            } catch (DeploymentException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
    }
}