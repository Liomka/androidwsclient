package io.liomka.androidwsclient.app;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.view.*;
import android.support.v4.widget.DrawerLayout;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import io.liomka.androidwsclient.app.worker.WebSocketsClient;
import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends ActionBarActivity implements
        NavigationDrawerFragment.NavigationDrawerCallbacks,
        ConnectionDialogFragment.ConnectionDialogCallbacks,
        WebSocketsClient.IWebSocketsClient {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment _navigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence _title;

    // Views from contentView
    private Menu _currentMenu;

    private View _inputMessageToSend;
    private View _sendButton;
    private ListView _chatContentListView;

    // Private components
    private ArrayAdapter<String> _chatContentListAdapter;
    private WebSocketsClient _webSocketsClient;


    /* -------------------------------------------------------------------------------------------------------------- */
    /* Activity overrides */
    /* -------------------------------------------------------------------------------------------------------------- */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        System.out.println("onCreate called");
        _navigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        _title = getTitle();

        // Set up the drawer.
        _navigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout)
        );

        // This is the array adapter, it takes the context of the activity as a
        // first parameter, the type of list view as a second parameter and your
        // array as a third parameter.
        _chatContentListAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1);

        // Get Views
        _chatContentListView = (ListView) findViewById(R.id.listView);
        _inputMessageToSend = findViewById(R.id.inputMessageToSend);
        _sendButton = findViewById(R.id.sendButton);

        // Init views
        _inputMessageToSend.setEnabled(false);
        _sendButton.setEnabled(false);
        _chatContentListView.setAdapter(_chatContentListAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!_navigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            _currentMenu = menu;
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return (id == R.id.action_settings) || super.onOptionsItemSelected(item);
    }


    /* --------------------------------------------------------------------- */
    /* ------------------------------ Callbacks ---------------------------- */
    /* --------------------------------------------------------------------- */

    // ConnectionDialogFragment
    @Override
    public void onConnect() {
        if (_currentMenu.findItem(R.id.action_connect).getTitle() == getString(R.string.action_connect)) {
            //_webSocketsClient = new WebSocketsClient("ws://pywsserver.herokuapp.com/ws");
            _webSocketsClient = new WebSocketsClient(this, "ws://10.10.40.181:5000/ws");
        } else {
            System.out.println("I want to kill");
            _webSocketsClient.kill();
        }
    }

    // NavigationDrawerFragment
    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
    }

    @Override
    public void onMenuConnectionTriggered() {
        onConnect();
        // TODO Restore the client connection dialog to server
        //ConnectionDialogFragment dialog = new ConnectionDialogFragment();
        //dialog.show(getSupportFragmentManager(), "NoticeDialogFragment");
    }

    /* ------------------------------ WebSockets --------------------------- */

    @Override
    public void onWebSocketsConnected(JSONObject time) {
        runOnUiThread(new Runnable () {

            @Override
            public void run() {
                _inputMessageToSend.setEnabled(true);
                _sendButton.setEnabled(true);
                _currentMenu.findItem(R.id.action_connect).setTitle(R.string.action_disconnect);
            }
        });
    }

    @Override
    public void onWebSocketsDisconnect(JSONObject time) {
        runOnUiThread(new Runnable () {

            @Override
            public void run() {
                _inputMessageToSend.setEnabled(false);
                _sendButton.setEnabled(false);
                _currentMenu.findItem(R.id.action_connect).setTitle(R.string.action_connect);
            }
        });
    }

    /**
     * Send the message to the ListView.
     * The given message
     * @param message SHOULD contain "name" and "content" keys.
     */
    @Override
    public void onMessageReceived(final JSONObject message) {
        runOnUiThread(new Runnable () {

            @Override
            public void run() {
                if (_chatContentListAdapter != null) {
                    try {
                        String name = message.getJSONObject("from").getString("name");
                        String content = message.getString("content");
                        _chatContentListAdapter.add(name + ": " + content);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    /* -------------------------------------------------------------------------------------------------------------- */
    /* Methods for layout file */
    /* -------------------------------------------------------------------------------------------------------------- */

    public void onSendButtonClicked(View view) {
        if (_webSocketsClient != null) {
            _webSocketsClient.sendMessage(((EditText) findViewById(R.id.inputMessageToSend)).getText().toString());
        }
    }


    /* -------------------------------------------------------------------------------------------------------------- */
    /* Methods */
    /* -------------------------------------------------------------------------------------------------------------- */

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                _title = getString(R.string.title_section1);
                break;
            case 2:
                _title = getString(R.string.title_section2);
                break;
            case 3:
                _title = getString(R.string.title_section3);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(_title);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_main, container, false);
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER)
            );
        }
    }
}
